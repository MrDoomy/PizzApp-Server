module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
    jest: true
  },
  extends: 'eslint:recommended',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  plugins: ['prettier', 'jest'],
  parserOptions: {
    ecmaVersion: 2019
  },
  rules: {
    'prettier/prettier': 'warn',
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'warn'
  }
};
