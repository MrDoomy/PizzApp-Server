const { verify } = require('jsonwebtoken');
const tokenModel = require('../models/tokenModel');

/**
 * Add Token
 *
 * @param {String} secret Secret
 */
const addToken = secret => {
  const expirationDate = new Date();
  expirationDate.setTime(expirationDate.getTime() + 43200); // NOTE: 12h

  tokenModel.create({ secret, expirationDate }).catch(err => {
    console.log(err);
  });
};

/**
 * Get All Tokens
 *
 * @returns {Promise} Tokens
 */
const getAllTokens = () => {
  return tokenModel.find({}).then(results => {
    return results.map(({ secret }) => secret);
  });
};

/**
 * Get Token
 *
 * @returns {Promise} Token
 */
const getToken = secret => {
  return tokenModel.findOne({ secret }).then(({ expirationDate }) => ({
    secret,
    expirationDate
  }));
};

/**
 * Del Token
 *
 * @param {String} secret Secret
 */
const delToken = secret => {
  getToken(secret).then(({ expirationDate }) => {
    const now = new Date();

    if (now.getTime() > expirationDate.getTime()) {
      tokenModel.findOneAndDelete({ secret }).catch(err => {
        console.log(err);
      });
    }
  });
};

/**
 * Verify Token
 *
 * @param {String} secret Secret
 * @param {Boolean} isLogout Is Logout (Default: 'false')
 */
const verifyToken = (secret, isLogout = false) => (req, res, next) => {
  const bearer = req.headers['authorization'];

  if (!bearer) {
    return res.status(403).send();
  }

  const token = bearer.replace(/Bearer\s/g, '');

  return getAllTokens().then(tokens => {
    if (tokens.includes(token)) {
      delToken(token);

      return res.status(403).send();
    }

    if (isLogout) {
      addToken(token);
    }

    verify(token, secret, (err, decoded) => {
      if (err) {
        return res.status(500).send(err);
      }

      req.decodedId = decoded.id;
      next();
    });
  });
};

/**
 * Reset Tokens
 */
const resetTokens = () => {
  getAllTokens().then(tokens => {
    tokens.forEach(token => {
      delToken(token);
    });
  });
};

module.exports = {
  verifyToken,
  resetTokens
};
