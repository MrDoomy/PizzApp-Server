const { compareSync, hashSync } = require('bcryptjs');
const { sign } = require('jsonwebtoken');
const accountModel = require('../models/accountModel');
const { isFileExist, readJsonFile, isDefined, bsonToJson, isMissing } = require('../utils');
const { HELLO_WORLD, NO_ACCOUNT, PSWD_FAILURE, IS_EMPTY } = require('../constants');

/**
 * Initialize Accounts
 * Read JSON & Feed DB
 *
 * @param {String} fileName FileName
 */
const initAccounts = (fileName = 'accounts.json') => {
  if (isFileExist(fileName)) {
    const accounts = readJsonFile(fileName);

    accounts.forEach(account => {
      const { login, password, email, firstName, lastName, gender, yearOld } = account;

      accountModel
        .findOne({ login })
        .then(result => {
          if (!result) {
            const hashedPswd = hashSync(password, 10);

            accountModel
              .create({
                ...account,
                password: hashedPswd
              })
              .catch(err => {
                console.log(err);
              });
          } else {
            result.email = email;
            result.firstName = firstName;
            result.lastName = lastName;
            result.gender = gender;
            result.yearOld = yearOld;

            result.save(err => {
              if (isDefined(err)) {
                console.log(err);
              }
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

/**
 * Register Account
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const registerAccount = ({ body }, res) => {
  const { login, email, password, firstName, lastName, gender, yearOld } = body;

  if (!isDefined(login)) {
    return res.status(400).json({ message: isMissing('login') });
  }

  if (!isDefined(email)) {
    return res.status(400).json({ message: isMissing('email') });
  }

  if (!isDefined(password)) {
    return res.status(400).json({ message: isMissing('password') });
  }

  if (!isDefined(firstName)) {
    return res.status(400).json({ message: isMissing('firstName') });
  }

  if (!isDefined(lastName)) {
    return res.status(400).json({ message: isMissing('lastName') });
  }

  if (!isDefined(gender)) {
    return res.status(400).json({ message: isMissing('gender') });
  }

  if (!isDefined(yearOld)) {
    return res.status(400).json({ message: isMissing('yearOld') });
  }

  const hashedPswd = hashSync(password, 10);

  return accountModel
    .create({
      login,
      email,
      password: hashedPswd,
      firstName,
      lastName,
      gender,
      yearOld
    })
    .then(result => {
      const token = sign({ id: bsonToJson(result).id }, HELLO_WORLD, {
        expiresIn: 43200 // NOTE: 12h
      });

      return res.status(201).json({ token });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Login Account
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const loginAccount = ({ body }, res) => {
  const { login, password } = body;

  if (!isDefined(login)) {
    return res.status(400).json({ message: isMissing('login') });
  }

  if (!isDefined(password)) {
    return res.status(400).json({ message: isMissing('password') });
  }

  return accountModel
    .findOne({ login })
    .then(result => {
      if (result) {
        const comparedPswd = compareSync(password, result.password);

        if (comparedPswd) {
          const token = sign({ id: bsonToJson(result).id }, HELLO_WORLD, {
            expiresIn: 43200 // NOTE: 12h
          });

          return res.status(200).json({ token });
        }

        return res.status(401).json({ message: PSWD_FAILURE });
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json(err);
    });
};

/**
 * Logout Account
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const logoutAccount = ({ decodedId: id }, res) => {
  return accountModel
    .findById(id, { password: 0 })
    .then(result => {
      if (result) {
        return res.status(200).json({ token: null });
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Change Account PSWD (With Decoded ID)
 * REST Method: PUT
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const pswdAccount = ({ decodedId: id, body }, res) => {
  const { oldPswd, newPswd } = body;

  if (!isDefined(oldPswd)) {
    return res.status(400).json({ message: isMissing('oldPswd') });
  }

  if (!isDefined(newPswd)) {
    return res.status(400).json({ message: isMissing('newPswd') });
  }

  return accountModel
    .findById(id)
    .then(result => {
      if (result) {
        const comparedPswd = compareSync(oldPswd, result.password);

        if (comparedPswd) {
          const hashedPswd = hashSync(newPswd, 10);

          return accountModel
            .findByIdAndUpdate(id, { password: hashedPswd })
            .then(() => {
              return res.status(200).json({ updatedId: bsonToJson(result).id });
            })
            .catch(err => {
              return res.status(500).json(err);
            });
        }

        return res.status(401).json({ message: PSWD_FAILURE });
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Get All Accounts
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getAllAccounts = (_, res) => {
  return accountModel
    .find({})
    .then(results => {
      return res.status(200).json(results.map(({ login, password }) => ({ login, password })));
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Get Account (With Decoded ID)
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getAccount = ({ decodedId: id }, res) => {
  return accountModel
    .findById(id, { password: 0 })
    .then(result => {
      if (result) {
        // eslint-disable-next-line
        const { id, ...data } = bsonToJson(result);

        return res.status(200).json(data);
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Update Account (With Decoded ID)
 * REST Method: PUT
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const updateAccount = ({ decodedId: id, body }, res) => {
  if (Object.entries(body).length === 0) {
    return res.status(400).json({ message: IS_EMPTY });
  }

  // CLEARFIX: Don't Save Pswd
  delete body.password;

  return accountModel
    .findByIdAndUpdate(id, body)
    .then(result => {
      if (result) {
        return res.status(200).json({ updatedId: bsonToJson(result).id });
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Clear Account (With Decoded ID)
 * REST Method: DELETE
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const clearAccount = ({ decodedId: id }, res) => {
  return accountModel
    .findByIdAndDelete(id)
    .then(result => {
      if (result) {
        return res.status(200).json({ deletedId: bsonToJson(result).id });
      }

      return res.status(404).json({ message: NO_ACCOUNT });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

module.exports = {
  initAccounts,
  registerAccount,
  loginAccount,
  logoutAccount,
  pswdAccount,
  getAllAccounts,
  getAccount,
  updateAccount,
  clearAccount
};
