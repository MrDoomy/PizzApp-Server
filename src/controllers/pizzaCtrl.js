const pizzaModel = require('../models/pizzaModel');
const { isFileExist, readJsonFile, isDefined, bsonToJson, isMissing } = require('../utils');
const { NOT_AN_ARRAY, NO_PIZZA, IS_EMPTY } = require('../constants');

/**
 * Initialize Pizzas
 * Read JSON & Feed DB
 *
 * @param {String} fileName FileName
 */
const initPizzas = (fileName = 'pizzas.json') => {
  if (isFileExist(fileName)) {
    const pizzas = readJsonFile(fileName);

    pizzas.forEach(pizza => {
      const { label, items, price } = pizza;

      pizzaModel
        .findOne({ label })
        .then(result => {
          if (!result) {
            pizzaModel.create(pizza).catch(err => {
              console.log(err);
            });
          } else {
            result.items = items;
            result.price = price;

            result.save(err => {
              if (isDefined(err)) {
                console.log(err);
              }
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

/**
 * Create Many Pizzas (From Array)
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const createManyPizzas = ({ body }, res) => {
  if (!Array.isArray(body)) {
    return res.status(400).json({ message: NOT_AN_ARRAY });
  }

  return pizzaModel
    .insertMany(body)
    .then(results => {
      return res.status(201).json({ createdCount: results.length });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Create Pizza (From Object)
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const createPizza = ({ body }, res) => {
  const { label, items, price } = body;

  if (!isDefined(label)) {
    return res.status(400).json({ message: isMissing('label') });
  }

  if (!isDefined(items)) {
    return res.status(400).json({ message: isMissing('items') });
  }

  if (!isDefined(price)) {
    return res.status(400).json({ message: isMissing('price') });
  }

  return pizzaModel
    .create({
      label,
      items,
      price
    })
    .then(result => {
      return res.status(201).json({ createdId: bsonToJson(result).id });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Get All Pizzas
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getAllPizzas = (_, res) => {
  return pizzaModel
    .find({})
    .then(results => {
      return res.status(200).json(results.map(result => bsonToJson(result)));
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Get Pizza (With ID)
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getPizza = ({ params }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).json({ message: isMissing('ID') });
  }

  return pizzaModel
    .findById(id)
    .then(result => {
      if (result) {
        return res.status(200).json(bsonToJson(result));
      }

      return res.status(404).json({ message: NO_PIZZA });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Update Pizza (With ID)
 * REST Method: PUT
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const updatePizza = ({ params, body }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).json({ message: isMissing('ID') });
  }

  if (Object.entries(body).length === 0) {
    return res.status(400).json({ message: IS_EMPTY });
  }

  return pizzaModel
    .findByIdAndUpdate(id, body)
    .then(result => {
      if (result) {
        return res.status(200).json({ updatedId: bsonToJson(result).id });
      }

      return res.status(404).json({ message: NO_PIZZA });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Clear All Pizzas
 * REST Method: DELETE
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const clearAllPizzas = (_, res) => {
  return pizzaModel
    .deleteMany({})
    .then(results => {
      return res.status(200).json({ deletedCount: results.deletedCount });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

/**
 * Clear Pizza
 * REST Method: DELETE
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const clearPizza = ({ params }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).json({ message: isMissing('ID') });
  }

  return pizzaModel
    .findByIdAndDelete(id)
    .then(result => {
      if (result) {
        return res.status(200).json({ deletedId: bsonToJson(result).id });
      }

      return res.status(404).json({ message: NO_PIZZA });
    })
    .catch(err => {
      return res.status(500).json(err);
    });
};

module.exports = {
  initPizzas,
  createManyPizzas,
  createPizza,
  getAllPizzas,
  getPizza,
  updatePizza,
  clearAllPizzas,
  clearPizza
};
