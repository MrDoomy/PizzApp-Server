const chalk = require('chalk');
const express = require('express');
const cors = require('cors');
const { json } = require('body-parser');
const { serve, setup } = require('swagger-ui-express');
const {
  registerAccount,
  loginAccount,
  logoutAccount,
  pswdAccount,
  getAllAccounts,
  getAccount,
  updateAccount,
  clearAccount
} = require('./controllers/accountCtrl');
const { createManyPizzas, createPizza, getAllPizzas, getPizza, updatePizza, clearAllPizzas, clearPizza } = require('./controllers/pizzaCtrl');
const { verifyToken } = require('./controllers/tokenCtrl');
const { getAddresses } = require('./utils');
const { BLUE, HELLO_WORLD } = require('./constants');
const swagger = require('./swagger.json');

const ASCII_ART = `
 _____ _
|  __ (_)           /\\
| |__) | ________  /  \\   _ __  _ __
|  ___/ |_  /_  / / /\\ \\ | '_ \\| '_ \\
| |   | |/ / / / / ____ \\| |_) | |_) |
|_|   |_/___/___/_/    \\_\\ .__/| .__/
                         | |   | |
                         |_|   |_|
`;

/**
 * Web Server
 */
function webServer(port, color) {
  const server = express();

  applyMiddleware();
  mountRoutes();

  /**
   * Apply Middleware
   * CORS / JSON / URL Encode
   */
  function applyMiddleware() {
    server.use(cors());
    server.use(json());
  }

  /**
   * Mount Routes
   * Account(s) API
   * Pizza(s) API
   */
  function mountRoutes() {
    // Account
    server.post('/api/account/register', registerAccount);
    server.post('/api/account/login', loginAccount);
    server.get('/api/account/logout', verifyToken(HELLO_WORLD, true), logoutAccount);
    server.put('/api/account/pswd', verifyToken(HELLO_WORLD), pswdAccount);

    server.get('/api/accounts', getAllAccounts);

    server
      .route('/api/account')
      .get(verifyToken(HELLO_WORLD), getAccount)
      .put(verifyToken(HELLO_WORLD), updateAccount)
      .delete(verifyToken(HELLO_WORLD), clearAccount);

    // Pizza
    server.post('/api/pizza', createPizza);

    server
      .route('/api/pizzas')
      .post(createManyPizzas)
      .get(getAllPizzas)
      .delete(clearAllPizzas);

    server
      .route('/api/pizza/:id')
      .get(getPizza)
      .put(updatePizza)
      .delete(clearPizza);

    server.use('/api-docs', serve, setup(swagger));
  }

  /**
   * Launch Server
   * Display...
   */
  function launchServer() {
    const log = text => console.log(text);

    /**
     * Set Chalk (Native / Hex)
     */
    const setChalk = color => text => {
      if (color.substring(0, 1) === '#') {
        return log(chalk.hex(color).bold(text));
      }

      return log(chalk[color].bold(text));
    };

    const colorizeLog = setChalk(color);

    server.listen(port, () => {
      colorizeLog(ASCII_ART);
      console.log('Specs:\n');
      getAddresses().forEach(address => {
        console.log(`- ${chalk.hex(BLUE)(`http://${address}:${port}/api-docs`)}`);
      });
    });
  }

  return {
    launchServer
  };
}

module.exports = webServer;
