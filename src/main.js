const chalk = require('chalk');
const mongoose = require('mongoose');
const webServer = require('./webServer');
const { initAccounts } = require('./controllers/accountCtrl');
const { initPizzas } = require('./controllers/pizzaCtrl');
const { resetTokens } = require('./controllers/tokenCtrl');
const { GREEN, YELLOW, RED } = require('./constants');

const app = webServer(process.env.PORT || 5050, process.env.COLOR || YELLOW);

// MongoDB Config
const { host, port, db } = {
  host: 'localhost',
  port: 27017,
  db: 'pizz-app'
};

const uri = `mongodb://${host}:${port}/${db}`;

// MongoDB Instance
mongoose
  .connect(uri, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    initAccounts();
    initPizzas();
    resetTokens();
    app.launchServer();
    console.log(`[DB] ${chalk.hex(GREEN)('Success !')}`);
  })
  .catch(() => {
    console.log(`[DB] ${chalk.hex(RED)('Failed...')}`);
  });
