const { Schema, model } = require('mongoose');

/**
 * Account Model
 *
 * @param {String} login Login
 * @param {String} email Email
 * @param {String} password Password
 * @param {String} firstName First Name
 * @param {String} lastName Last Name
 * @param {Array} gender Gender
 * @param {Number} yearOld Year Old
 */
const accountSchema = new Schema(
  {
    login: {
      type: String,
      unique: true,
      minlength: 4,
      maxlength: 32
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true
    },
    password: {
      type: String,
      minlength: 6
    },
    firstName: {
      type: String,
      minlength: 2,
      maxlength: 16
    },
    lastName: {
      type: String,
      minlength: 2,
      maxlength: 16
    },
    gender: {
      type: String,
      enum: ['M', 'F']
    },
    yearOld: {
      type: Number,
      min: 18,
      max: 99
    }
  },
  {
    versionKey: false
  }
);

module.exports = model('accounts', accountSchema);
