const { connect, clear, disconnect } = require('../setupTests');
const pizzaModel = require('./pizzaModel');

describe('pizzaModel', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  it('Should Be Created', async () => {
    const result = await pizzaModel.create({
      label: 'Lorem Ipsum',
      items: ['One', 'Two', 'Three'],
      price: 9.99
    });

    expect(result.label).toEqual('Lorem Ipsum');
    expect(result.items).toHaveLength(3);
    expect(result.price).toEqual(9.99);
  });
});
