# PizzApp

> _Made With **NodeJS** 12_

## Intro

Server based on pizza management for accessing to the **NoSQL** database.

This _backend_ application was build using following technologies,

- ExpressJS
- Mongoose

## File Structure

```
.
+-- src
    +-- constants
        +-- index.js
    +-- controllers
        +-- acccountCtrl.js
        +-- acccountCtrl.spec.js
        +-- pizzaCtrl.js
        +-- pizzaCtrl.spec.js
        +-- tokenCtrl.js
    +-- models
        +-- accountModel.js
        +-- accountModel.spec.js
        +-- pizzaModel.js
        +-- pizzaModel.spec.js
        +-- tokenModel.js
    +-- utils
        +-- index.js
    +-- accounts.json
    +-- main.js
    +-- pizzas.json
    +-- setupTests.js
    +-- swagger.json
    +-- webServer.js
+-- .eslintrc.js
+-- .gitignore
+-- .prettierrc
+-- jest.config.js
+-- LICENSE
+-- package.json
+-- README.md
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/dmnchzl/pizzapp-server.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Launch (_As Service_):

```
npm run watch
```

Test:

```
npm run test
```

Lint:

```
npm run lint --fix
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
